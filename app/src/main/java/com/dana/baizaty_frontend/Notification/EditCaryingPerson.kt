package com.dana.baizaty_frontend.Notification

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.dana.baizaty_frontend.R
import com.dana.baizaty_frontend.singleton.AppSingleton
import kotlinx.android.synthetic.main.activity_edit_carying_person.*
import org.json.JSONObject

class EditCaryingPerson : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_carying_person)

        var intent = intent

        var id = intent.getStringExtra("id")
        var name = intent.getStringExtra("name")
        var relationship = intent.getStringExtra("relation")
        var status = intent.getStringExtra("status").toInt()
        var amount = intent.getStringExtra("amount")

        AddedID.setText(id)
        AddedName.setText(name)
        AddedRelation.setText(relationship)

        if (status == 1) {
            AddedStatus.setText("Active")

            ActiveSwitch.setText("Active")
            ActiveSwitch.setChecked(true)
        } else {
            AddedStatus.setText("Inactive")

            ActiveSwitch.setText("Inactive")
            ActiveSwitch.setChecked(false)
        }

        AddedAmount.setText(amount)


        DoneButton.setOnClickListener {
            var newstatus = ActiveSwitch.isChecked
            var statusvalue = 0



            if (newstatus == false) {
                statusvalue = 0
            } else {
                statusvalue = 1
            }

            var newAmount = AddedAmount.text
            var newputAmount = ""

            if ( newAmount.toString() == amount) {
                newputAmount = amount
            }else{
                newputAmount = newAmount.toString()
            }


            val requestParams = HashMap<String,Any>()

            requestParams["amount"] = newputAmount
            requestParams["status"] = statusvalue
            requestParams["id"] = id
            val paramsJsonObject = JSONObject(requestParams as Map<*, *>)




            var queue = AppSingleton.instance.getQueue(this)

            var ModifyUser = JsonObjectRequest(Request.Method.POST, AppSingleton.instance.getBaseUrl()
                    + "peopleWhoCare/updatePeople", paramsJsonObject, Response.Listener {

                // Success case
                Toast.makeText(this, "$name Modified !", Toast.LENGTH_LONG).show()

                finish()
            }, Response.ErrorListener {
                // Error case
                Toast.makeText(this, "Internet connection error!", Toast.LENGTH_LONG).show()
            })

            queue?.add(ModifyUser)
        }




        DeleteButton.setOnClickListener{

            var queue = AppSingleton.instance.getQueue(this)


            var ModifyUser = StringRequest(Request.Method.GET, AppSingleton.instance.getBaseUrl()
                    + "peopleWhoCare/deletePeople/$id", Response.Listener<String> {

                // Success case
                Toast.makeText(this, "$name Deleted !", Toast.LENGTH_LONG).show()

                finish()
            }, Response.ErrorListener {
                // Error case
                Toast.makeText(this, "Internet connection error!", Toast.LENGTH_LONG).show()
            })

            queue?.add(ModifyUser)
        }
    }
}

