package com.dana.baizaty_frontend.Notification


import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import android.widget.Toast
import com.dana.baizaty_frontend.R
import personWhoCare
import java.util.ArrayList


class PeopleWhoCareAdapter (var careList: ArrayList<personWhoCare>, var activity: Activity): BaseAdapter(){

    override fun getView(pos: Int, p1: View?, p2: ViewGroup?): View {

        var v = activity.layoutInflater.inflate(R.layout.item_people_who_care,null)

        var care = careList.get(pos)

        var vAccNo  = v.findViewById<TextView>(R.id.AccountNoTextView)
        vAccNo.text = care?.id.toString()

        var vName  = v.findViewById<TextView>(R.id.NameTextView)
        if ( care?.relationship.equals("Mother")) {
            vName.text="Mother"
        } else if (care?.relationship.equals("Father") ){
            vName.text = "Father"
        } else {
            vName.text = care?.name
        }

        var vstatus  = v.findViewById<TextView>(R.id.statusvTextView)

        if (care?.status == 0) {
            vstatus.text = "Inactive"
            vstatus.setTextColor(Color.parseColor("#D22630"))

        } else{
            vstatus.text = "Active"
            vstatus.setTextColor(Color.parseColor("#4BD864"))
        }

        var vAmount  = v.findViewById<TextView>(R.id.AmountnoTextView)
        vAmount.text = care?.amount.toString()

        v.setOnClickListener{
            var intent = Intent(activity, EditCaryingPerson()::class.java)
            Toast.makeText(activity, "View ${vName.text}", Toast.LENGTH_LONG).show()

            intent.putExtra("id",care.id.toString())
            intent.putExtra("name",care.name)

            intent.putExtra("relation",care.relationship)
            intent.putExtra("status",care.status.toString())

            intent.putExtra("amount",care.amount.toString())

            activity.startActivity(intent)
        }

        return v
    }

    override fun getItem(p0: Int): Any {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getItemId(p0: Int): Long {

        return p0.toLong()
    }

    // Get the List of ingredeient size
    override fun getCount(): Int {
        return careList.size
    }
}


