

data class personWhoCare (

	val id : Int,
	val owner_id : Int,
	val name : String,
	val relationship : String,
	val status : Int,
	val amount : Int,
	val email : String,
	val phone : Int
)