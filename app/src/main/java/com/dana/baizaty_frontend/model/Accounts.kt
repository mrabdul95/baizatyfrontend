package com.dana.baizaty_frontend.model

data class Accounts(
    var id: Int,
    var owner_id: Int,
    var type: String,
    var balance: Double
)