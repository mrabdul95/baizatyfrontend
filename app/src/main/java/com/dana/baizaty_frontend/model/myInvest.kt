package com.dana.baizaty_frontend.model

class myInvest (


    val id : Int,
    val owner_id : Int,
    val amount : Double,
    val company : String,
    val rate : Double,
    val duration : Int
)