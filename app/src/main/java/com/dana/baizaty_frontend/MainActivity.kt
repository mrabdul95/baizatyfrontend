package com.dana.baizaty_frontend
import android.Manifest
import android.content.pm.PackageManager
import com.dana.baizaty_frontend.adapter.ViewPagerAdapter
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        homeViewPager.adapter = ViewPagerAdapter(supportFragmentManager)
//        viewPager.adapter = adapter
//        dotsIndicator.setViewPager(viewPager)
        if (intent.getBooleanExtra("LaunchDietPlan", false)) {
            homeViewPager.currentItem = 1
        }

        homeTablayout.setupWithViewPager(homeViewPager)


        homeTablayout.getTabAt(0)?.setIcon(R.mipmap.white_bell)
        homeTablayout.getTabAt(1)?.setIcon(R.mipmap.pie)
        homeTablayout.getTabAt(2)?.setIcon(R.mipmap.red_home_icon)
        homeTablayout.getTabAt(3)?.setIcon(R.mipmap.loan_lending)
        homeTablayout.getTabAt(4)?.setIcon(R.mipmap.arrow)

        ckeckPerm()

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            //R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }




    }


fun ckeckPerm(){

    if (ContextCompat.checkSelfPermission(this,
            Manifest.permission.SEND_SMS)
        != PackageManager.PERMISSION_GRANTED) {

        // Permission is not granted
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.SEND_SMS)) {
            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
        } else {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.SEND_SMS),
                1)

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    } else {
        // Permission has already been granted
    }
}

}

