package com.dana.baizaty_frontend.adapter

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import android.widget.Toast
import com.dana.baizaty_frontend.Notification.EditCaryingPerson
import com.dana.baizaty_frontend.R
import com.dana.baizaty_frontend.model.myInvest
import java.util.ArrayList

class InvestmentAdapter (var careList: ArrayList<myInvest>, var activity: Activity): BaseAdapter(){

    override fun getView(pos: Int, p1: View?, p2: ViewGroup?): View {

        var v = activity.layoutInflater.inflate(R.layout.item_investment,null)

        var care = careList.get(pos)

        var vAccNo  = v.findViewById<TextView>(R.id.id_invest)
        vAccNo.text = ""+care?.id.toString()

        var vName  = v.findViewById<TextView>(R.id.editText_organization)
        vName.text = care?.company.toString()

        var vstatus  = v.findViewById<TextView>(R.id.editText3_Rate)
        vstatus.text = ""+care?.rate.toString()+" %"

        var vAmount  = v.findViewById<TextView>(R.id.editText2_Amount)
        vAmount.text = ""+care?.amount.toString()+" kd"

        var vcommit = v.findViewById<TextView>(R.id.editText4_commit)
        vcommit.text = ""+care?.duration.toString()+" Months"

//        v.setOnClickListener{
//            var intent = Intent(activity, EditCaryingPerson()::class.java)
//            Toast.makeText(activity, "View ${vName.text}", Toast.LENGTH_LONG).show()
//
//            intent.putExtra("id",care.id.toString())
//            intent.putExtra("name",care.name)
//
//            intent.putExtra("relation",care.relationship)
//            intent.putExtra("status",care.status.toString())
//
//            intent.putExtra("amount",care.amount.toString())
//
//            activity.startActivity(intent)
//        }

        return v
    }

    override fun getItem(p0: Int): Any {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getItemId(p0: Int): Long {

        return p0.toLong()
    }

    // Get the List of ingredeient size
    override fun getCount(): Int {
        return careList.size
    }
}
