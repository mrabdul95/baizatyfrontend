package com.dana.baizaty_frontend

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ListView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.dana.baizaty_frontend.Notification.PeopleWhoCareAdapter
import com.dana.baizaty_frontend.adapter.InvestmentAdapter
import com.dana.baizaty_frontend.model.myInvest
import com.dana.baizaty_frontend.singleton.AppSingleton
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_my_investment_.*
import java.util.ArrayList

class My_investment_Activity : AppCompatActivity() {

    override fun onResume() {
        super.onResume()
        vol()

    }

    private lateinit var lv: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_investment_)

        lv=myListviewInvest


        vol()
    }
    fun vol(){

        var queue = AppSingleton.instance.getQueue(this)

        var careURL = "http://206.189.179.71:3000/invest/4"

        var CareRequest = StringRequest(Request.Method.GET, careURL,
            Response.Listener<String> { response ->

                var careList = Gson().fromJson<ArrayList<myInvest>>(response,
                    object: TypeToken<ArrayList<myInvest>>(){}.type)

                // it'll send the AnswerList array to the Ingredient Addapter with the fragment Acitvity

                lv.adapter= this?.let {
                    InvestmentAdapter(careList, it) }


            }, Response.ErrorListener {
                // Error case
                Log.d("PersonERROR",it.toString())
            })  //push

        queue?.add(CareRequest)
    }
}
