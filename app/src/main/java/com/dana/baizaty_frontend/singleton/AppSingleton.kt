package com.dana.baizaty_frontend.singleton
//
//import VerifyLoginResponse
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

internal class AppSingleton private constructor() {

//    private val imagesDirectory: String = "http://138.68.102.173/images/"

    private var queue: RequestQueue? = null
    private val baseUrl: String = "http://206.189.179.71:3000/"
    // for the tokenizer
    private var myPreferences: SharedPreferences? = null
    private var sharedPrefFile = "com.pifss.sharedpref"

//    fun getImagesDirectory(): String {
//
//        return imagesDirectory
//
//    }

    fun getQueue(context: Context?): RequestQueue? {

        if (queue == null) queue = Volley.newRequestQueue(context)
        return queue

    }

    fun getBaseUrl(): String {

        return baseUrl

    }

//    fun saveToken(context: Context, token: String) {
//        myPreferences = PreferenceManager.getDefaultSharedPreferences(context)
//        val preferencesEditor = myPreferences?.edit()
//        preferencesEditor?.putString("token", token)
//        preferencesEditor?.apply()
//    }

//    fun readToken(context: Context) : String {
//        myPreferences = PreferenceManager.getDefaultSharedPreferences(context)
//        val token = myPreferences?.getString("token", "")!!
//
//        return token
//    }

//    fun clearToken(context: Context) {
//        myPreferences = PreferenceManager.getDefaultSharedPreferences(context)
//        val preferencesEditor = myPreferences?.edit()
//        preferencesEditor?.clear()
//        preferencesEditor?.apply()
//    }

    companion object {

        val instance = AppSingleton()

    }

}
