package com.dana.baizaty_frontend

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.SmsManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_gargish_act.*

class GargishActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gargish_act)

        button_calculate?.setOnClickListener {
            if (editText_amount.text.toString()==""||editText_people.text.toString()=="")

                Toast.makeText(it.context,"Please fill up all fields",Toast.LENGTH_LONG).show()

            else{


                var total = editText_amount.text.toString()
                var people = editText_people.text.toString()
                var individual = total.toDouble() / people.toDouble()

                viewJameia.setText("Each will pay : " + individual.toString() + "  kd\n" + "Payment links valid for " + editText_timetopay.text.toString() + " days")
            }
            //Toast.makeText(it.context,editText_total.text,Toast.LENGTH_LONG).show()
        }
        button_submit?.setOnClickListener {

            sendSMS()

//            Toast.makeText(it.context,"Success!!",Toast.LENGTH_LONG).show()
//            val intent = Intent(this, MainActivity::class.java)
//            startActivity(intent)
        }
    }

   private fun sendSMS()
    {

        val smsManager = SmsManager.getDefault() as SmsManager
        smsManager.sendTextMessage("+96599623221", null, "عميلنا العزيز , تم إضافتك إلى قرقش الرجاء دفع المبلغ من خلال الرابط المتاح", null, null)

        Toast.makeText(this,"Success!!",Toast.LENGTH_LONG).show()
    }

}
